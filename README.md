# Bike service center management system - documentation
All files like project overview, database layer design, and interface design are in the "documentation" folder.  
There is a link to Figma where mockups, wireframes and prototype are created: https://www.figma.com/file/fdWnjcqIssdszADPDAj8Fk/Capstone-Project?node-id=2%3A181

## Technologies

For this project I have used:
- Java 11
- MySQL 8.0
- Maven
- Tomcat
- HTML
- CSS
- Javascript

## Requirements:

- Apache Tomcat 9.0
- MySQL 8.0.30

In MySQL run these commands in order to create database for web app (by default username and password for the database are set to "root" and "password", if you would like to change it, go to "src/main/webapp/META-INF/context.xml"):
```
CREATE DATABASE `bike_servicedb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE `bike` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `client_ID` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_ID_idx` (`client_ID`),
  CONSTRAINT `client_ID_bike` FOREIGN KEY (`client_ID`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idclient_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `order_part` (
  `order_part_ID` int NOT NULL AUTO_INCREMENT,
  `order_ID` int NOT NULL,
  `part_ID` int NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`order_part_ID`),
  KEY `order_ID_idx` (`part_ID`),
  KEY `order_ID_idx1` (`order_ID`),
  CONSTRAINT `order_ID` FOREIGN KEY (`order_ID`) REFERENCES `orders` (`order_ID`),
  CONSTRAINT `part_ID` FOREIGN KEY (`part_ID`) REFERENCES `part` (`part_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orders` (
  `order_ID` int NOT NULL AUTO_INCREMENT,
  `client_ID` int NOT NULL,
  `bike_ID` int NOT NULL,
  `repair_description` varchar(500) NOT NULL,
  `collect_date` date NOT NULL,
  `service_price` decimal(10,0) NOT NULL,
  `order_state` enum('NEW','STARTED','FINISHED','COLLECTED') NOT NULL,
  PRIMARY KEY (`order_ID`),
  KEY `client_ID_idx` (`client_ID`),
  KEY `bike_ID_idx` (`bike_ID`),
  CONSTRAINT `bike_ID_order` FOREIGN KEY (`bike_ID`) REFERENCES `bike` (`id`),
  CONSTRAINT `client_ID_order` FOREIGN KEY (`client_ID`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `part` (
  `part_ID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  PRIMARY KEY (`part_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
  `user_ID` int NOT NULL AUTO_INCREMENT,
  `login` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`user_ID`),
  UNIQUE KEY `user_ID_UNIQUE` (`user_ID`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_order` (
  `user_ID` int NOT NULL,
  `order_ID` int NOT NULL,
  KEY `user_id_user_order_idx` (`user_ID`),
  KEY `order_ID_user_order_idx` (`order_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```
After setting up database, deploy WAR to your tomcat, run it, and open website via link:
http://localhost:8080/bike-service-center-management-system/controller

### Copyrights
Logo used for website is designed by rawpixel.com / Freepik.

