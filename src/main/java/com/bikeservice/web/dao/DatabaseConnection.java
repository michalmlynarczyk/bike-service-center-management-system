package com.bikeservice.web.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseConnection {
    public static Connection getConnection() {
        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/bike_servicedb");
            return ds.getConnection();
        } catch (SQLException | NamingException e) {
            throw new RuntimeException(e);
        }
    }
}
