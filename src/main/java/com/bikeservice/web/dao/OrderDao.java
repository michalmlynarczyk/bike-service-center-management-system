package com.bikeservice.web.dao;

import com.bikeservice.web.entities.OrderDbImpl;
import com.bikeservice.web.entities.OrderState;

import java.util.List;
import java.util.Optional;

public interface OrderDao extends BaseDao<OrderDbImpl, Integer> {
    void update(Integer orderId, OrderState state);

    Optional<List<OrderDbImpl>> getAll(Integer userId);

    Optional<OrderDbImpl> getById(Integer orderId);

    void connectOrderWithUser(Integer orderId, Integer userId);
}
