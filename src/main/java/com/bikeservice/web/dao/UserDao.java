package com.bikeservice.web.dao;

import com.bikeservice.web.entities.User;

import java.util.Optional;

public interface UserDao extends BaseDao<User, Integer> {
    Optional<User> getByLoginAndPassword(String login, String password);

    Optional<User> getByLogin(String login);
}
