package com.bikeservice.web.dao;

import com.bikeservice.web.entities.Part;

import java.util.List;
import java.util.Optional;

public interface PartDao extends BaseDao<Part, Integer> {
    Optional<Part> getByNameAndPrice(String name, Double price);
    void connectPartWithOrder(Integer partId, Integer orderId, Integer quantity);
    List<Part> getAllOrderParts(Integer orderId);
}
