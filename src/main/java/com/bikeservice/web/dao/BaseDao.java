package com.bikeservice.web.dao;

public interface BaseDao<T, Id> {
    void add(T t);
}
