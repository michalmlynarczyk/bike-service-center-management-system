package com.bikeservice.web.dao.daoImpl;

import com.bikeservice.web.dao.DatabaseConnection;
import com.bikeservice.web.dao.UserDao;
import com.bikeservice.web.entities.User;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.Optional;

public class UserDaoImpl implements UserDao {
    final String INSERT = "INSERT INTO user (login,password) VALUES (?,?)";
    final String SELECTBYLOGINANDPASSWORD = "SELECT * FROM user WHERE login=? AND password=?";
    final String SELECTBYLOGIN = "SELECT * FROM user WHERE login=?";
    @Override
    public Optional<User> getByLoginAndPassword(String login, String password) throws RuntimeException {
        User user = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYLOGINANDPASSWORD)) {
                String passwd = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
                statement.setString(1, login);
                statement.setString(2, passwd);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    user = new User(rs.getInt(1), rs.getString(2), rs.getString(3));
                }
                rs.close();
                return Optional.ofNullable(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> getByLogin(String login) throws RuntimeException {
        User user = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYLOGIN)) {
                statement.setString(1, login);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    user = new User(rs.getInt(1), rs.getString(2), rs.getString(3));
                }
                rs.close();
                return Optional.ofNullable(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(User user) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                String passwd = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
                statement.setString(1, user.getLogin());
                statement.setString(2, passwd);
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    user.setId(pk);
                }
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
