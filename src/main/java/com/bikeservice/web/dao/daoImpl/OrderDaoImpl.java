package com.bikeservice.web.dao.daoImpl;

import com.bikeservice.web.dao.DatabaseConnection;
import com.bikeservice.web.dao.OrderDao;
import com.bikeservice.web.entities.OrderDbImpl;
import com.bikeservice.web.entities.OrderState;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDaoImpl implements OrderDao {
    final String INSERT = "INSERT INTO orders (client_ID,bike_ID,repair_description,collect_date,service_price,order_state) " +
            "VALUES(?,?,?,?,?,?)";
    final String UPDATE = "UPDATE orders SET order_state = ? WHERE order_ID = ?";
    final String SELECTALL = "SELECT * FROM orders WHERE order_ID IN (SELECT order_ID FROM user_order WHERE user_ID = ?)";
    final String SELECTBYID = "SELECT * FROM orders WHERE order_ID=?";
    final String INSERTUSERORDER = "INSERT INTO user_order (user_ID,order_ID) VALUES (?,?)";

    @Override
    public void add(OrderDbImpl order) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                statement.setInt(1, order.getClient());
                statement.setInt(2, order.getBike());
                statement.setString(3, order.getDescription());
                statement.setDate(4, order.getDate());
                statement.setBigDecimal(5, BigDecimal.valueOf(order.getServicePrice()));
                statement.setString(6, order.getState().name());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    order.setId(pk);
                }
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Integer orderId, OrderState state) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(UPDATE)) {
                statement.setString(1, state.name());
                statement.setInt(2, orderId);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<List<OrderDbImpl>> getAll(Integer userId) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTALL)) {
                ArrayList<OrderDbImpl> orderList = new ArrayList<>();
                statement.setInt(1, userId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    orderList.add(getOrderInstance(rs));
                }

                return Optional.of(orderList);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<OrderDbImpl> getById(Integer orderId) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYID)) {
                OrderDbImpl order = null;
                statement.setInt(1, orderId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    order = getOrderInstance(rs);
                }

                return Optional.ofNullable(order);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void connectOrderWithUser(Integer orderId, Integer userId) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERTUSERORDER)) {
                statement.setInt(1, userId);
                statement.setInt(2, orderId);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private OrderDbImpl getOrderInstance(ResultSet rs) throws SQLException {
        int orderId = rs.getInt(1);
        int clientId = rs.getInt(2);
        int bikeId = rs.getInt(3);
        String description = rs.getString(4);
        Date date = rs.getDate(5);
        Double servicePrice = rs.getDouble(6);
        OrderState state = OrderState.valueOf(rs.getString(7));
        return new OrderDbImpl(orderId, clientId, bikeId, description, date, servicePrice, state);
    }
}

