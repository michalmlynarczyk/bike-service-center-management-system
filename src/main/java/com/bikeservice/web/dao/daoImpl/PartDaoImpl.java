package com.bikeservice.web.dao.daoImpl;

import com.bikeservice.web.dao.DatabaseConnection;
import com.bikeservice.web.dao.PartDao;
import com.bikeservice.web.entities.Part;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PartDaoImpl implements PartDao {
    final String INSERT = "INSERT INTO part (name,price) VALUES (?,?)";
    final String SELECTBYNAMEANDPRICE = "SELECT * FROM part WHERE name = ? AND price =?";
    final String INSERTORDERPART = "INSERT INTO order_part (order_ID,part_ID,quantity) VALUES (?,?,?)";
    final String SELECTORDERPARTS = "SELECT * FROM part JOIN order_part ON part.part_ID = order_part.part_ID WHERE order_ID = ?;";

    @Override
    public void add(Part part) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, part.getName());
                statement.setDouble(2, part.getPrice());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    part.setId(pk);
                }
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Part> getByNameAndPrice(String name, Double price) {
        Part part = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYNAMEANDPRICE, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, name);
                statement.setDouble(2, price);
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    part = new Part(pk, name, price);
                }
                rs.close();
                return Optional.ofNullable(part);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void connectPartWithOrder(Integer partId, Integer orderId, Integer quantity) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERTORDERPART)) {
                statement.setInt(1, orderId);
                statement.setInt(2, partId);
                statement.setInt(3, quantity);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Part> getAllOrderParts(Integer orderId) {
        List<Part> partBeanList = new ArrayList<>();
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTORDERPARTS)) {
                statement.setInt(1, orderId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    partBeanList.add(new Part(rs.getInt(1), rs.getString(2), rs.getDouble(3),
                            rs.getInt(7)));
                }
                rs.close();
                return partBeanList;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
