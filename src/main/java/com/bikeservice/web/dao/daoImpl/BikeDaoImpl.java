package com.bikeservice.web.dao.daoImpl;

import com.bikeservice.web.dao.BikeDao;
import com.bikeservice.web.dao.DatabaseConnection;
import com.bikeservice.web.entities.Bike;

import java.sql.*;
import java.util.Optional;

public class BikeDaoImpl implements BikeDao {
    final String INSERT = "INSERT INTO bike (brand,model,color,client_ID) VALUES (?,?,?,?)";
    final String SELECTBYID = "SELECT * FROM bike WHERE id=?";
    final String SELECTBYNAME = "SELECT * FROM bike WHERE brand = ? AND model =? AND color = ? AND client_ID = ?";

    @Override
    public void add(Bike bike) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, bike.getBrand());
                statement.setString(2, bike.getModel());
                statement.setString(3, bike.getColor());
                statement.setInt(4, bike.getClientId());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    bike.setId(pk);
                }
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Bike getById(Integer bikeId) {
        Bike bike = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYID)) {
                statement.setInt(1, bikeId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    bike = getBikeInstance(bikeId, rs);
                }
                rs.close();
                return bike;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Bike> findBikeByNameAndClient(String brand, String model, String color, Integer clientId) {
        Bike bike = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYNAME, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, brand);
                statement.setString(2, model);
                statement.setString(3, color);
                statement.setInt(4, clientId);
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    bike = new Bike(pk, clientId, brand, model, color);
                }
                rs.close();
                return Optional.ofNullable(bike);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Bike getBikeInstance(int bikeId, ResultSet rs) throws SQLException {
        String brand = rs.getString(2);
        String model = rs.getString(3);
        String color = rs.getString(4);
        int clientId = rs.getInt(5);
        return new Bike(bikeId, clientId, brand, model, color);
    }
}
