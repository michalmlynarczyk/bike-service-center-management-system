package com.bikeservice.web.dao.daoImpl;

import com.bikeservice.web.dao.ClientDao;
import com.bikeservice.web.dao.DatabaseConnection;
import com.bikeservice.web.entities.Client;

import java.sql.*;
import java.util.Optional;

public class ClientDaoImpl implements ClientDao {
    final String INSERT = "INSERT INTO client (first_name,last_name,phone_number) VALUES (?,?,?)";
    final String SELECTBYID = "SELECT * FROM client WHERE id=?";
    final String SELECTBYNAME = "SELECT * FROM client WHERE first_name = ? AND last_name =? AND phone_number = ?";

    @Override
    public void add(Client client) {
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, client.getFirstName());
                statement.setString(2, client.getLastName());
                statement.setString(3, client.getPhoneNumber());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    client.setId(pk);
                }
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Client getById(Integer clientId) {
        Client client = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYID)) {
                statement.setInt(1, clientId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    client = getClientInstance(rs);
                }
                rs.close();
                return client;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Client> getByNameAndNumber(String fName, String lName, String phoneNumber) {
        Client client = null;
        try (Connection conn = DatabaseConnection.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECTBYNAME, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, fName);
                statement.setString(2, lName);
                statement.setString(3, phoneNumber);
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    int pk = rs.getInt(1);
                    client = new Client(pk, fName, lName, phoneNumber);
                }
                rs.close();
                return Optional.ofNullable(client);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Client getClientInstance(ResultSet rs) throws SQLException {
        int clientId = rs.getInt(1);
        String fName = rs.getString(2);
        String lName = rs.getString(3);
        String phoneNumber = rs.getString(4);
        return new Client(clientId, fName, lName, phoneNumber);
    }
}
