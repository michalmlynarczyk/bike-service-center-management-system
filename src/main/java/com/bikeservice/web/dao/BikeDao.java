package com.bikeservice.web.dao;

import com.bikeservice.web.entities.Bike;

import java.util.Optional;

public interface BikeDao extends BaseDao<Bike, Integer> {
    Bike getById(Integer bikeId);
    Optional<Bike> findBikeByNameAndClient(String brand, String model, String color, Integer clientId);
}
