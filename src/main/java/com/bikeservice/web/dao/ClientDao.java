package com.bikeservice.web.dao;

import com.bikeservice.web.entities.Client;

import java.util.Optional;

public interface ClientDao extends BaseDao<Client, Integer> {
    Client getById(Integer clientId);
    Optional<Client> getByNameAndNumber(String fName, String lName, String phoneNumber);
}
