package com.bikeservice.web.service;

import com.bikeservice.web.dao.daoImpl.UserDaoImpl;
import com.bikeservice.web.entities.User;

import java.sql.SQLException;
import java.util.Optional;

public class UserService {
    UserDaoImpl userDao = new UserDaoImpl();

    public Optional<User> login(String login, String password) throws SQLException {
        return userDao.getByLoginAndPassword(login, password);
    }

    public Optional<User> registerUser(String login, String password) {
        if (userDao.getByLogin(login).isPresent()) return Optional.empty();
        User user = new User(0, login, password);
        userDao.add(user);
        return Optional.of(user);
    }
}
