package com.bikeservice.web.service;

import com.bikeservice.web.dao.daoImpl.BikeDaoImpl;
import com.bikeservice.web.dao.daoImpl.ClientDaoImpl;
import com.bikeservice.web.dao.daoImpl.OrderDaoImpl;
import com.bikeservice.web.dao.daoImpl.PartDaoImpl;
import com.bikeservice.web.entities.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OrderService {
    OrderDaoImpl orderDao = new OrderDaoImpl();
    BikeDaoImpl bikeDao = new BikeDaoImpl();
    ClientDaoImpl clientDao = new ClientDaoImpl();
    PartDaoImpl partDao = new PartDaoImpl();

    public OrderDbImpl addOrder(int clientId, int bikeId, String description, Double servicePrice, OrderState state, int userId) throws RuntimeException {
        java.sql.Date date = Date.valueOf(LocalDate.now());
        OrderDbImpl order = new OrderDbImpl(0, clientId, bikeId, description, date, servicePrice, state);
        orderDao.add(order);
        connect(order.getId(), userId);
        return order;
    }

    public List<OrderBeanImpl> getAllOrders(int userId) {
        List<OrderBeanImpl> orderBeanList = new ArrayList<>();
        Optional<List<OrderDbImpl>> orderList = orderDao.getAll(userId);
        orderList.ifPresent(orders -> orders.forEach(order -> {
            Bike bike = bikeDao.getById(order.getBike());
            Client client = clientDao.getById(order.getClient());
            orderBeanList.add(new OrderBeanImpl(order.getId(), client, bike, order.getDescription(), order.getDate()
                    , order.getServicePrice(), order.getState()));
        }));
        return orderBeanList;
    }

    public OrderBeanImpl getOrderById(int userId) {
        Optional<OrderDbImpl> order = orderDao.getById(userId);
        OrderBeanImpl orderBean = null;
        if (order.isPresent()) {
            OrderDbImpl ord = order.get();
            Bike bike = bikeDao.getById(ord.getBike());
            Client client = clientDao.getById(ord.getClient());
            orderBean = new OrderBeanImpl(ord.getId(), client, bike, ord.getDescription(), ord.getDate()
                    , ord.getServicePrice(), ord.getState());
        }
        return orderBean;
    }

    public void updateOrderState(int orderId, String state) {
        OrderState stateEnum = OrderState.valueOf(state);
        orderDao.update(orderId, stateEnum);
    }

    public void connect(int orderId, int userId) {
        orderDao.connectOrderWithUser(orderId, userId);
    }

    public int addClient(String fName, String lName, String phoneNumber) {
        Optional<Client> client = clientDao.getByNameAndNumber(fName, lName, phoneNumber);
        if (client.isPresent()) {
            return client.get().getId();
        } else {
            Client newClient = new Client(0, fName, lName, phoneNumber);
            clientDao.add(newClient);
            return newClient.getId();
        }
    }

    public List<Client> getClients(int userId) {
        List<OrderBeanImpl> orderListDistinctClient = getAllOrders(userId).stream()
                .filter(distinctClientById(order -> (order.getClient()).getId())).collect(Collectors.toList());
        List<Client> clientList = new ArrayList<>();
        orderListDistinctClient.forEach(order -> clientList.add(order.getClient()));
        return clientList;
    }

    public <T> Predicate<T> distinctClientById(
            Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public int addBike(String brand, String model, String color, int clientId) {
        Optional<Bike> bike = bikeDao.findBikeByNameAndClient(brand, model, color, clientId);
        if (bike.isPresent()) {
            return bike.get().getId();
        } else {
            Bike newBike = new Bike(0, clientId, brand, model, color);
            bikeDao.add(newBike);
            return newBike.getId();
        }
    }

    public void addParts(String[] partName, String[] partPrice, String[] partQuantity, int orderId) {
        for (int i = 0; i < partName.length; i++) {
            String name = partName[i];
            Double price = Double.parseDouble(partPrice[i]);
            int quantity = Integer.parseInt(partQuantity[i]);
            Optional<Part> partFromDb = partDao.getByNameAndPrice(name, price);
            int partId;
            if (partFromDb.isEmpty()) {
                Part newPart = new Part(0, name, price);
                partDao.add(newPart);
                partId = newPart.getId();
            } else {
                partId = partFromDb.get().getId();
            }
            partDao.connectPartWithOrder(partId, orderId, quantity);
        }
    }

    public List<Part> getAllOrderParts(int orderId) {
        return partDao.getAllOrderParts(orderId);
    }

    public Double getTotalPrice(OrderBeanImpl order, List<Part> parts) {
        Double servicePrice = order.getServicePrice();
        Double partsPrice = parts.stream().mapToDouble(part -> part.getPrice() * part.getQuantity()).sum();
        return Math.floor((partsPrice + servicePrice) * 100) / 100;
    }
}
