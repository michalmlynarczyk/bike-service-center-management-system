package com.bikeservice.web.command;

public class CommandFactory {
    public Command create(String commandParameter) {
        CommandEnum command = CommandEnum.valueOf(commandParameter);
        return command.getInstance();
    }
}
