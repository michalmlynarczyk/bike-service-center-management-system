package com.bikeservice.web.command;

public interface CommandInstance {
    Command getInstance();
}
