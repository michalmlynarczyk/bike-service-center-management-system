package com.bikeservice.web.command;

import com.bikeservice.web.command.commandImpl.*;

public enum CommandEnum implements CommandInstance {
    LOGIN {
        @Override
        public Command getInstance() {
            return new CommandLogin();
        }
    },
    LOGOUT {
        @Override
        public Command getInstance() {
            return new CommandLogout();
        }
    },
    NEW_ORDER {
        @Override
        public Command getInstance() {
            return new CommandShowNewOrderPage();
        }
    },
    SHOW_ORDERS {
        @Override
        public Command getInstance() {
            return new CommandShowOrdersPage();
        }
    },
    SHOW_CLIENTS {
        @Override
        public Command getInstance() {
            return new CommandShowClientsPage();
        }
    },
    ADD_NEW_ORDER {
        @Override
        public Command getInstance() {
            return new CommandAddNewOrder();
        }
    },
    SHOW_ORDER_DETAILS {
        @Override
        public Command getInstance() {
            return new CommandShowOrderDetails();
        }
    },
    UPDATE_ORDER {
        @Override
        public Command getInstance() {
            return new CommandUpdateOrder();
        }
    },
    ERROR {
        @Override
        public Command getInstance() {
            return new CommandError();
        }
    }
}