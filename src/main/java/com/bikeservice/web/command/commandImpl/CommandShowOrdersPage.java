package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.entities.OrderBeanImpl;
import com.bikeservice.web.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

public class CommandShowOrdersPage implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ServletException {
        if (req.getSession().getAttribute("userId") != null) {
            OrderService service = new OrderService();
            List<OrderBeanImpl> orderList = service.getAllOrders((Integer) req.getSession().getAttribute("userId"));
            req.setAttribute("orders", orderList);
            return "orders.jsp";
        } else {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
    }
}