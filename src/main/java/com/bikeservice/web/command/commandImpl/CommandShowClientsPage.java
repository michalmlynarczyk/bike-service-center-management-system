package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.entities.Client;
import com.bikeservice.web.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CommandShowClientsPage implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getSession().getAttribute("userId") != null) {
            OrderService service = new OrderService();
            List<Client> clientList = service.getClients((Integer)req.getSession().getAttribute("userId"));
            req.setAttribute("clients", clientList);
            return "clients.jsp";
        } else {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
    }
}
