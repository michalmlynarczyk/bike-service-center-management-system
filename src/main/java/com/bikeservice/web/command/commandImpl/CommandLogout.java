package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandLogout implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp){
        req.getSession().invalidate();
        return "login.jsp";
    }
}
