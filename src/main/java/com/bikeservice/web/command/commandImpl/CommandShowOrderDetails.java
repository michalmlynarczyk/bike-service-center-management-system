package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.entities.OrderBeanImpl;
import com.bikeservice.web.entities.Part;
import com.bikeservice.web.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

public class CommandShowOrderDetails implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ServletException {
        if (req.getSession().getAttribute("userId") != null) {
            int orderId = Integer.parseInt(req.getParameter("orderIdToUpdate"));
            OrderService service = new OrderService();
            OrderBeanImpl order = service.getOrderById(orderId);
            List<Part> parts = service.getAllOrderParts(orderId);
            Double total = service.getTotalPrice(order, parts);
            req.setAttribute("order", order);
            req.setAttribute("parts", parts);
            req.setAttribute("totalPrice", total);
            return "orderUpdate.jsp";
        } else {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
    }
}
