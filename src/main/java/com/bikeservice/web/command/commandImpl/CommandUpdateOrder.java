package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class CommandUpdateOrder implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ServletException {
        if (req.getSession().getAttribute("userId") != null) {
            int orderId = Integer.parseInt(req.getParameter("orderIdToUpdate"));
            String state = req.getParameter("orderStateUpdate");
            OrderService service = new OrderService();
            service.updateOrderState(orderId, state);
            return "controller?command=show_orders";
        } else {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
    }
}
