package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.entities.OrderDbImpl;
import com.bikeservice.web.entities.OrderState;
import com.bikeservice.web.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandAddNewOrder implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getSession().getAttribute("userId") == null) {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
        OrderService service = new OrderService();
        int clientId = addClientAndGetClientId(req, service);
        int bikeId = addBikeAndGetBikeId(req, service, clientId);
        OrderDbImpl order = addOrder(req, service, clientId, bikeId);

        if (req.getParameterMap().containsKey("partName")) {
            addParts(req, service, order.getId());
        }
        return "controller?command=show_orders";
    }

    private void addParts(HttpServletRequest req, OrderService service, int orderId) {
        String[] parts = req.getParameterValues("partName");
        String[] partPrice = req.getParameterValues("partPrice");
        String[] partQuantity = req.getParameterValues("partQuantity");
        service.addParts(parts, partPrice, partQuantity, orderId);
    }


    private OrderDbImpl addOrder(HttpServletRequest req, OrderService service, int clientId, int bikeId) {
        String description = req.getParameter("description");
        Double servicePrice = Double.parseDouble(req.getParameter("servicePrice"));
        OrderState state = OrderState.valueOf(req.getParameter("orderState"));
        int userId = (int) req.getSession().getAttribute("userId");
        return service.addOrder(clientId, bikeId, description, servicePrice, state, userId);
    }

    private int addBikeAndGetBikeId(HttpServletRequest req, OrderService service, int clientId) {
        String brand = req.getParameter("brand");
        String model = req.getParameter("model");
        String color = req.getParameter("color");
        return service.addBike(brand, model, color, clientId);
    }

    private int addClientAndGetClientId(HttpServletRequest req, OrderService service) {
        String fName = req.getParameter("firstName");
        String lName = req.getParameter("lastName");
        String phoneNumber = req.getParameter("phoneNumber");
        return service.addClient(fName, lName, phoneNumber);
    }
}
