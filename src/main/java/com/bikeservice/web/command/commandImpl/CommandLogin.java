package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.entities.User;
import com.bikeservice.web.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.Optional;

public class CommandLogin implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        boolean isNewUser = req.getParameterMap().containsKey("isNewUser");
        if (req.getSession(false) != null && login == null && password == null) {
            return "controller?command=show_orders";
        }
        if (login == null || password == null) {
            req.setAttribute("error", "Mandatory parameter is missing");
            return "login.jsp";
        }

        UserService service = new UserService();

        if (isNewUser) {
            Optional<User> user = service.registerUser(login, password);
            if (user.isEmpty()) {
                req.setAttribute("error", "Login is already taken");
                return "login.jsp";
            }
            return successfulLogin(req, user.get());
        } else {
            Optional<User> user = service.login(login, password);
            if (user.isPresent()) {
                return successfulLogin(req, user.get());
            }
            req.setAttribute("error", "Login or password is invalid");
            return "login.jsp";
        }
    }

    private String successfulLogin(HttpServletRequest req, User user) {
        req.getSession().setAttribute("userId", user.getId());
        return "controller?command=show_orders";
    }
}
