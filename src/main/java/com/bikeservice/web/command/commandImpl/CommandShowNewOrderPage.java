package com.bikeservice.web.command.commandImpl;

import com.bikeservice.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class CommandShowNewOrderPage implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ServletException {
        if (req.getSession().getAttribute("userId") != null) {
            return "neworder.jsp";
        } else {
            req.setAttribute("error", "Please log in");
            return "login.jsp";
        }
    }
}
