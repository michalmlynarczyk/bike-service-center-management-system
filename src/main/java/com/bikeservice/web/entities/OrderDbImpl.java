package com.bikeservice.web.entities;


import java.sql.Date;

public class OrderDbImpl implements OrderDb {
    private Integer id;
    private Integer clientId;
    private Integer bikeId;
    private String description;
    private java.sql.Date date;
    private Double servicePrice;
    private OrderState state;


    public OrderDbImpl(int id, int clientId, int bikeId, String description, Date date, Double servicePrice, OrderState state) {
        this.id = id;
        this.clientId = clientId;
        this.bikeId = bikeId;
        this.description = description;
        this.date = date;
        this.servicePrice = servicePrice;
        this.state = state;
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Double getServicePrice() {
        return this.servicePrice;
    }

    @Override
    public void setServicePrice(Double servicePrice) {
        this.servicePrice = servicePrice;
    }

    @Override
    public OrderState getState() {
        return this.state;
    }

    @Override
    public void setState(OrderState state) {
        this.state = state;
    }

    @Override
    public Integer getClient() {
        return clientId;
    }

    @Override
    public void setClient(Integer clientId) {
        this.clientId = clientId;
    }

    @Override
    public Integer getBike() {
        return bikeId;
    }

    @Override
    public void setBike(Integer bikeId) {
        this.bikeId = bikeId;
    }
}

