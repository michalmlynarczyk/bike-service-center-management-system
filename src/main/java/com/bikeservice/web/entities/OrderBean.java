package com.bikeservice.web.entities;

public interface OrderBean extends BaseOrder {
    Client getClient();

    void setClient(Client client);

    Bike getBike();

    void setBike(Bike bike);
}
