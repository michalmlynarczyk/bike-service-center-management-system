package com.bikeservice.web.entities;

import java.io.Serializable;
import java.sql.Date;

public class OrderBeanImpl implements OrderBean, Serializable {
    private Integer id;
    private Client client;
    private Bike bike;
    private String description;
    private Date date;
    private Double servicePrice;
    private OrderState state;

    public OrderBeanImpl(int id, Client client, Bike bike, String description, Date date, Double servicePrice, OrderState state) {
        this.id = id;
        this.client = client;
        this.bike = bike;
        this.description = description;
        this.date = date;
        this.servicePrice = servicePrice;
        this.state = state;
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public java.sql.Date getDate() {
        return this.date;
    }

    @Override
    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    @Override
    public Double getServicePrice() {
        return this.servicePrice;
    }

    @Override
    public void setServicePrice(Double servicePrice) {
        this.servicePrice = servicePrice;
    }

    @Override
    public OrderState getState() {
        return this.state;
    }

    @Override
    public void setState(OrderState state) {
        this.state = state;
    }

    @Override
    public Client getClient() {
        return client;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public Bike getBike() {
        return bike;
    }

    @Override
    public void setBike(Bike bike) {
        this.bike = bike;
    }
}
