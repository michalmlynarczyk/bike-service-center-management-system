package com.bikeservice.web.entities;

public enum OrderState {
    NEW,
    STARTED,
    FINISHED,
    COLLECTED
}
