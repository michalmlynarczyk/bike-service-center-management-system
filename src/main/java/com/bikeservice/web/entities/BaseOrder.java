package com.bikeservice.web.entities;


import java.sql.Date;

public interface BaseOrder {
    int getId();

    void setId(int id);

    String getDescription();

    void setDescription(String description);

    Date getDate();

    void setDate(java.sql.Date date);

    Double getServicePrice();

    void setServicePrice(Double servicePrice);

    OrderState getState();

    void setState(OrderState state);

}
