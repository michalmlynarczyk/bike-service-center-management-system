package com.bikeservice.web.entities;

import java.io.Serializable;

public class Bike implements Serializable {
    private int id;
    private int clientId;
    private String brand;
    private String model;
    private String color;

    public Bike(int id, int clientId, String brand, String model, String color) {
        this.id = id;
        this.clientId = clientId;
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
