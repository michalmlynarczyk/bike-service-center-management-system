package com.bikeservice.web.entities;

public interface OrderDb extends BaseOrder {
    Integer getClient();

    void setClient(Integer clientId);

    Integer getBike();

    void setBike(Integer bikeId);
}
