package com.bikeservice.web;

import com.bikeservice.web.command.Command;
import com.bikeservice.web.command.CommandFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Controller extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            process(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            process(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, SQLException {
        if (!req.getParameterMap().containsKey("command")) {
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        } else {
            String commandParameter = req.getParameter("command").toUpperCase();
            CommandFactory factory = new CommandFactory();
            Command command = factory.create(commandParameter);
            String page = command.execute(req, resp);
            req.getRequestDispatcher(page).forward(req, resp);
        }
    }
}
