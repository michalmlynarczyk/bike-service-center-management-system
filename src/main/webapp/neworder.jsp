<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/styles.css">
    <title>Bike service: Add new ticket</title>
</head>

<body>
    <nav>
        <img src="static/images/logo.jpg" alt="logo">
        <ul>
            <li>
                <a href="controller?command=new_order">Add new ticket</a>
            </li>
            <li>
                <a class="currentPage" href="controller?command=show_orders">Orders</a>
            </li>
            <li>
                <a href="controller?command=show_clients">Clients</a>
            </li>
            <li>
                <a href="controller?command=logout">logout</a>
            </li>
        </ul>
    </nav>
    <section>
        <form action="controller?command=add_new_order" method="post">
            <p class="boardTitle newTicketTitle">Client</p>
            <ul class="clientInput">
                <li class="left">
                    <input class="text border" type="text" placeholder="First name" name="firstName" required>
                </li>
                <li>
                    <input class="text border" type="text" placeholder="Last name" name="lastName" required>
                </li>
                <li class="right">
                    <input class="text border" type="text" placeholder="Phone number" name="phoneNumber" required>
                </li>
            </ul>
            <p class="boardTitle newTicketTitle">Bike</p>
            <ul class="clientInput">
                <li class="left">
                    <input class="text border" type="text" placeholder="Brand" name="brand" required>
                </li>
                <li>
                    <input class="text border" type="text" placeholder="Model" name="model" required>
                </li>
                <li class="right">
                    <input class="text border" type="text" placeholder="Color" name="color" required>
                </li>
            </ul>
            <p class="boardTitle newTicketTitle">Parts</p>
            <button class="border addPartBtn" type="button" onclick='addInput()'>add part</button>
            <div id="partsContainer">

            </div>
            <p class="boardTitle newTicketTitle">Repair details</p>
            <div class="repairWrapper">
                <div class="repairDescription">
                    <input class="text border" type="text" name="description" placeholder="Description" required>
                </div>
                <div class="repairPrice">
                    <input class="border" type="number" name="servicePrice" min="1" placeholder="Service price"
                        step="0.01" required>
                </div>
            </div>
            <div class="repairWrapper repairStatus">
                <p class="boardTitle newTicketTitle">status</p>
                <select class="border" name="orderState" id="status">
                    <option value="NEW">new</option>
                    <option value="STARTED">started</option>
                    <option value="FINISHED">finished</option>
                    <option value="COLLECTED">collected by client</option>
                </select>
            </div>
            <div class="footer">
                <button class="border" type="submit">save</button>
            </div>
        </form>
    </section>
    <script>
        const container = document.getElementById('partsContainer');
        var maxInputAllowed = 5;
        var inputCount = 0;

        // Call addInput() function on button click
        function addInput() {
            let html =
                '<ul class="clientInput"><li class="left"><input class="text border" name="partName"type="text" placeholder="Name" required></li><li><input class="text border" name="partPrice" type="number" min="1" placeholder="Price" step="0.01" required></li><li class="right"><input class="text border" name="partQuantity" type="number" min="1" placeholder="Quantity" required></li></ul>';
            let range = document.createRange();
            let fragment = range.createContextualFragment(html);
            inputCount++; // Increment input count by one
            if (inputCount > 5) {
                alert('You can add maximum 5 input fields.');
                return;
            }
            container.appendChild(fragment);
        }
    </script>
</body>

</html>