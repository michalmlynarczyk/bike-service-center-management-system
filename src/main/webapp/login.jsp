<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/styles.css">
    <title>Bike Service: Login</title>
</head>

<body>
    <div class="wrapper">
        <div class="imgContainer">
            <img src="static/images/logo.jpg" alt="logo">
        </div>
        <div class="loginContainer">
            <h1 class="title">login to service center account</h1>
            <c:if test="${requestScope.error!=null}">
                <h2 class="title error">${error}</h2>
            </c:if>
            <form action="controller?command=login" method="post">
                <ul class="loginList pos">
                    <li>
                        <input class="text border" type="text" name="login" placeholder="Enter login" required>
                    </li>
                    <li>
                        <input class="text border" type="password" name="password" placeholder="Enter password"
                            required>
                    </li>
                    <li>
                        <input class="checkbox" type="checkbox" name="isNewUser" value="yes">
                        <p class="checkboxTitle">I'm new user</p>
                    </li>
                    <li>
                        <button class="pos border" type="submit">login</button>
                    </li>
                </ul>
            </form>

        </div>
    </div>
</body>

</html>