<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="e" uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" %>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/styles.css">
    <title>Bike service: Update order</title>
</head>

<body>
    <nav>
        <img src="static/images/logo.jpg" alt="logo">
        <ul>
            <li>
                <a href="controller?command=new_order">Add new ticket</a>
            </li>
            <li>
                <a class="currentPage" href="controller?command=show_orders">Orders</a>
            </li>
            <li>
                <a href="controller?command=show_clients">Clients</a>
            </li>
            <li>
                <a href="controller?command=logout">logout</a>
            </li>
        </ul>
    </nav>
    <section>
        <p class="boardTitle newTicketTitle">Client</p>
        <ul class="clientInput">
            <li class="left">
                <p class="label">First name</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getClient().getFirstName())}</p>
                </div>
            </li>
            <li>
                <p class="label">last name</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getClient().getLastName())}</p>
                </div>
            </li>
            <li class="right">
                <p class="label">phone number</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getClient().getPhoneNumber())}</p>
                </div>
            </li>
        </ul>
        <p class="boardTitle newTicketTitle">Bike</p>
        <ul class="clientInput">
            <li class="left">
                <p class="label">brand</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getBike().getBrand())}</p>
                </div>
            </li>
            <li>
                <p class="label">model</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getBike().getModel())}</p>
                </div>
            </li>
            <li class="right">
                <p class="label">color</p>
                <div class="text border">
                    <p class="dataDisplay">${e:forHtml(order.getBike().getColor())}</p>
                </div>
            </li>
        </ul>
        <p class="boardTitle newTicketTitle">Parts</p>
        <c:if test="${requestScope.parts != null}">
            <c:forEach items="${requestScope.parts}" var="part">

                <ul class="clientInput">
                    <li class="left">
                        <p class="label">part name</p>
                        <div class="text border">
                            <p class="dataDisplay">${e:forHtml(part.getName())}</p>
                        </div>
                    </li>
                    <li>
                        <p class="label">price</p>
                        <div class="text border">
                            <p class="dataDisplay">${part.getPrice()}$</p>
                        </div>
                    </li>
                    <li class="right">
                        <p class="label">quantity</p>
                        <div class="text border">
                            <p class="dataDisplay">${part.getQuantity()}</p>
                        </div>
                    </li>
                </ul>
            </c:forEach>
        </c:if>

        <p class="boardTitle newTicketTitle">Repair details</p>
        <div class="repairWrapper">
            <div class="repairDescription textContainer">
                <h2>Description</h2>
                <p>${e:forHtml(order.getDescription())}</p>
            </div>
            <div class="repairPrice">
                <p class="label total">total price</p>
                <div class="border">
                    <p>${totalPrice}$</p>
                </div>
            </div>
        </div>
        <div class="repairWrapper repairStatus">
            <p class="boardTitle newTicketTitle">status</p>
            <form action="controller?command=update_order" method="post">
                <input type="hidden" name="orderIdToUpdate" value=${order.getId()}>
                <select class="border" name="orderStateUpdate">
                    <option value="NEW">new</option>
                    <option value="STARTED">started</option>
                    <option value="FINISHED">finished</option>
                    <option value="COLLECTED">collected by client</option>
                </select>
        </div>
        <div class="footer">
            <button class="border" type="submit">save</button>
        </div>
        </form>
    </section>

</body>

</html>