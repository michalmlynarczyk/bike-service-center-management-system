<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="e" uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" %>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/styles.css">
    <title>Bike com.bikeservice.web.service: Orders</title>
</head>

<body>
    <nav>
        <img src="static/images/logo.jpg" alt="logo">
        <ul>
            <li>
                <a href="controller?command=new_order">Add new ticket</a>
            </li>
            <li>
                <a class="currentPage" href="controller?command=show_orders">Orders</a>
            </li>
            <li>
                <a href="controller?command=show_clients">Clients</a>
            </li>
            <li>
                <a href="controller?command=logout">logout</a>
            </li>
        </ul>
    </nav>
    <section class="statusBoard">
        <div class="new">
            <div class="sticky">
                <p class="boardTitle">new</p>
            </div>
            <c:if test="${requestScope.orders != null}">
                <c:forEach items="${requestScope.orders}" var="order">
                    <c:if test="${order.getState().name()=='NEW'}">
                        <form action="controller?command=show_order_details" method="post">
                            <input type="hidden" name="orderIdToUpdate" value=${order.getId()}>
                            <table class="ticket">
                                <tr>
                                    <td>Bike:</td>
                                    <td>${e:forHtml(order.getBike().getBrand())} ${e:forHtml(order.getBike().getModel())}</td>
                                </tr>
                                <tr>
                                    <td>Client:</td>
                                    <td>${e:forHtml(order.getClient().getFirstName())} ${e:forHtml(order.getClient().getLastName())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">${e:forHtml(order.getDate())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><button class="border details" type="submit">show details</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </c:if>
                </c:forEach>
            </c:if>
        </div>
        <div class="started">
            <div class="sticky">
                <p class="boardTitle">started</p>
            </div>
            <c:if test="${requestScope.orders != null}">
                <c:forEach items="${requestScope.orders}" var="order">
                    <c:if test="${order.getState().name()=='STARTED'}">
                        <form action="controller?command=show_order_details" method="post">
                            <input type="hidden" name="orderIdToUpdate" value=${order.getId()}>
                            <table class="ticket">
                                <tr>
                                    <td>Bike:</td>
                                    <td>${e:forHtml(order.getBike().getBrand())} ${e:forHtml(order.getBike().getModel())}</td>
                                </tr>
                                <tr>
                                    <td>Client:</td>
                                    <td>${e:forHtml(order.getClient().getFirstName())} ${e:forHtml(order.getClient().getLastName())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">${e:forHtml(order.getDate())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><button class="border details" type="submit">show details</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </c:if>
                </c:forEach>
            </c:if>
        </div>
        <div class="finished">
            <div class="sticky">
                <p class="boardTitle">finished</p>
            </div>
            <c:if test="${requestScope.orders != null}">
                <c:forEach items="${requestScope.orders}" var="order">
                    <c:if test="${order.getState().name()=='FINISHED'}">
                        <form action="controller?command=show_order_details" method="post">
                            <input type="hidden" name="orderIdToUpdate" value=${order.getId()}>
                            <table class="ticket">
                                <tr>
                                    <td>Bike:</td>
                                    <td>${e:forHtml(order.getBike().getBrand())} ${e:forHtml(order.getBike().getModel())}</td>
                                </tr>
                                <tr>
                                    <td>Client:</td>
                                    <td>${e:forHtml(order.getClient().getFirstName())} ${e:forHtml(order.getClient().getLastName())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">${e:forHtml(order.getDate())}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><button class="border details" type="submit">show details</button></td>
                                </tr>
                            </table>
                        </form>
                    </c:if>
                </c:forEach>
            </c:if>
        </div>
    </section>
</body>

</html>