<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="e" uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" %>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="static/css/styles.css">
    <title>Bike com.bikeservice.web.service: Clients</title>
</head>

<body>
    <nav>
        <img src="static/images/logo.jpg" alt="logo">
        <ul>
            <li>
                <a href="controller?command=new_order">Add new ticket</a>
            </li>
            <li>
                <a class="currentPage" href="controller?command=show_orders">Orders</a>
            </li>
            <li>
                <a href="controller?command=show_clients">Clients</a>
            </li>
            <li>
                <a href="controller?command=logout">logout</a>
            </li>
        </ul>
    </nav>
    <section>
        <table class="clientsTable">
            <tr>
                <th class="boardTitle">first name</th>
                <th class="boardTitle">last name</th>
                <th class="boardTitle">phone number</th>
            </tr>
            <c:if test="${requestScope.clients != null}">
                <c:forEach items="${requestScope.clients}" var="client">
                    <tr>
                        <td>${e:forHtml(client.getFirstName())}</td>
                        <td>${e:forHtml(client.getLastName())}</td>
                        <td>${e:forHtml(client.getPhoneNumber())}</td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
    </section>
</body>

</html>