package com.bikeservice.web.service;

import com.bikeservice.web.entities.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderServiceTest {

    @Test
    public void calculateTotalOrderPriceTest() {
        OrderBeanImpl order = new OrderBeanImpl(1,new Client(1,"fNameTest","lNameTest","123222")
        ,new Bike(1,1,"brandTest","modelTest","colorTest"),
                "test", Date.valueOf(LocalDate.now()),190.5, OrderState.FINISHED);
        List<Part> parts = new ArrayList<>();
        parts.add(new Part(1,"testName",20.5,2));
        parts.add(new Part(1,"testName",21.5,3));
        OrderService service = new OrderService();
        Double totalPrice = service.getTotalPrice(order,parts);
        Assertions.assertEquals(274.5,5,totalPrice);
    }
}